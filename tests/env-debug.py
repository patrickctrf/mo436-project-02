from numpy import moveaxis
from tensorforce import Agent, Runner

from futeamigos.environment.grids import ULTIMATE_GRID, BASIC_GRID, VERTICAL_GRID
from futeamigos.environment.robot_tag_v4 import RobotTagV4
from futeamigos.offpolicy.offpolicy import custom_reward

if __name__ == '__main__':
    env = RobotTagV4.from_grids(grids=[VERTICAL_GRID, ])
    env.reward_function = custom_reward

    print(ULTIMATE_GRID)
    observation = env.reset()

    obs_de_volta = moveaxis(observation, 2, 0)
    x = 1
