import time
from os import system

from futeamigos.environment import (CardinalDirections,
                                    DeterministicFollowerAgent, RobotTagV3)
from futeamigos.environment.grids import GRID_ROTATION

if __name__ == '__main__':
    env = RobotTagV3.from_grids(grids=GRID_ROTATION)

    follower_agent = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    for _ in range(10):
        observation = env.reset()

        done = False

        while not done:
            system('clear')
            env.render()
            time.sleep(0.5)

            observation, reward, done, world = env.step(
                actions=(
                    CardinalDirections.CENTER,
                    follower_agent.get_action(observation=observation)
                )
            )
