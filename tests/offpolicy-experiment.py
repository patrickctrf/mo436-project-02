import pickle

from tensorforce import Agent, Runner, Environment

from futeamigos.environment import DeterministicFollowerAgent, RobotTagV3
from futeamigos.environment.grids import ULTIMATE_GRID, BASIC_GRID
from futeamigos.environment.robot_tag_v4 import RobotTagV4
from futeamigos.environment.world import World
from futeamigos.offpolicy.offpolicy import DqnAgent, train


def custom_reward(world: World) -> float:
    """
Defining our custom reward according to our objective.

    :param world: World with environment information.
    :type world: World
    :return: Reward for current Q.
    :rtype: float
    """
    if world.player_1.touched(other=world.player_2):
        # We got caught
        return -15.0
    else:
        # We are still running away
        return 1.0


def test_callback(r, parallel=False):
    with open('rewards_test.csv', 'a') as csv_file:
        csv_file.write(f"{r.episode_rewards[-1]}\n")
    return True


def train_callback(r, parallel=False):
    if len(r.episode_rewards) % 100 == 0:
        avg_reward = sum(r.episode_rewards[-100:]) / 100
        with open('rewards_train.csv', 'a') as csv_file:
            csv_file.write(f"{avg_reward}\n")
    return True


if __name__ == '__main__':
    env = RobotTagV3.from_grids(grids=[ULTIMATE_GRID,])
    env.reward_function = custom_reward

    follower_agent = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    my_agent = DqnAgent(env)

    training_results = train(env, my_agent, follower_agent)

    pickle.dump(my_agent, open("my_agent.sklearn", 'wb'))

    print("Rewards:", training_results)



    exit()




    env = RobotTagV4.from_grids(grids=[BASIC_GRID, ])
    env.reward_function = custom_reward
    env.reset()

    env = Environment.create(environment="gym", level=env, max_episode_timesteps=200)
    agent = Agent.create(agent="dqn",
                         environment=env,
                         batch_size=1,
                         discount=0.9,
                         target_sync_frequency=10,
                         learning_rate=10 ** -3,
                         exploration=0.5,
                         network=[dict(type='conv2d', size=64, window=(5, 5)),
                                  dict(type='conv2d', size=64, window=(5, 5)),
                                  dict(type='flatten'),
                                  dict(type='dense', size=256, activation="relu"),
                                  dict(type='dense', size=128, activation="relu"),
                                  dict(type='dense', size=9, activation="none")],
                         memory=10 ** 5
                         )

    # Create the runner
    runner = Runner(agent=agent, environment=env)
    runner.run(num_episodes=10 ** 4, callback=train_callback)
    # # Start running
    # for i in range(100):
    #     runner.run(num_episodes=100, callback=train_callback)
    #     runner.run(num_episodes=20, evaluation=True, callback=test_callback)
    #     print("Rodada:", i)

    runner.run(num_episodes=20, evaluation=True, callback=test_callback)
    runner.close()

    agent.close()
