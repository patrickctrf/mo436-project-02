# Commands
MKDIR=mkdir
CP=cp -rfv
PYTHON=python3
RM=rm -rfv
CD=cd

# Folders
SRC_FOLDER=tests
OUTPUT_FOLDER=output
INPUT_FOLDER=futeamigos
CACHE_FOLDER=__pycache__

# Python scripts
PY_SOURCE=$(wildcard $(SRC_FOLDER)/*.py)

# Input assets
INPUTS=$(wildcard $(INPUT_FOLDER)/*)

all: run
	$(RM) $(subst $(SRC_FOLDER), $(OUTPUT_FOLDER), $(PY_SOURCE))
	$(RM) $(subst $(INPUT_FOLDER), $(OUTPUT_FOLDER), $(INPUTS))
	$(RM) $(addprefix $(OUTPUT_FOLDER)/, $(CACHE_FOLDER)) 
	$(RM) $(addprefix $(INPUT_FOLDER)/, $(CACHE_FOLDER))
	
build: $(PY_SOURCE) $(INPUTS) | $(OUTPUT_FOLDER)
	$(CP) $(PY_SOURCE) $(OUTPUT_FOLDER)
	$(CP) $(INPUTS) $(OUTPUT_FOLDER) 2> /dev/null || :
	
run: $(addprefix ./, $(subst $(SRC_FOLDER), $(OUTPUT_FOLDER), $(PY_SOURCE)))
	
exec: $(addprefix ./NO_BUILD/, $(subst $(SRC_FOLDER), $(OUTPUT_FOLDER), $(PY_SOURCE)))

$(OUTPUT_FOLDER):
	$(MKDIR) $(OUTPUT_FOLDER)
	
clean:
	$(RM) $(subst $(SRC_FOLDER), $(OUTPUT_FOLDER), $(PY_SOURCE))
	$(RM) $(subst $(INPUT_FOLDER), $(OUTPUT_FOLDER), $(INPUTS))
	
veryclean: 
	$(RM) $(OUTPUT_FOLDER)/*
	
./$(OUTPUT_FOLDER)/%.py: build
	$(CD) $(OUTPUT_FOLDER); \
	$(PYTHON) $(subst $(OUTPUT_FOLDER)/,,$@)
		
./NO_BUILD/$(OUTPUT_FOLDER)/%.py:
	$(CD) $(OUTPUT_FOLDER); \
	$(PYTHON) $(subst NO_BUILD/$(OUTPUT_FOLDER)/,,$@)
	

