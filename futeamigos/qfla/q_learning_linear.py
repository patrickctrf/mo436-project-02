import gym
import itertools
import matplotlib
import numpy as np
import sys
import sklearn.pipeline
import sklearn.preprocessing

from joblib import dump, load

from sklearn.linear_model import SGDRegressor
from sklearn.kernel_approximation import RBFSampler

from futeamigos.environment import (CardinalDirections,
                                    DeterministicFollowerAgent, RobotTagV2)
from futeamigos.environment.character import CharacterObservation

TEST_GRID = [
    '                   ',
    '                   ',
    '                   ',
    '                   ',
    '                   ',
    '                   '
]

ULTIMATE_GRID = [
    "W             W  ",
    "    W  W   W     ",
    "       W WWW  W W",
    "WW  W        WW  ",
    "    WW WWW      W",
    "WW         WWWW  ",
]


DIRS = [
    'NW',
    'N',
    'NE',
    'E',
    'SE',
    'S',
    'SW',
    'W',
    'C',
]


class QLearningLFAAgent:
    def __init__(self, env):
        self.env = env

        observation_examples = np.array([np.clip(env.observation_space.sample(), -10, 10) for x in range(10000)])
        self.scaler = sklearn.preprocessing.StandardScaler()
        self.scaler.fit(observation_examples)

        # Used to convert a state to a featurizes represenation.
        # We use RBF kernels with different variances to cover different parts of the space
        self.featurizer = sklearn.pipeline.FeatureUnion([
                ("rbf1", RBFSampler(gamma=5.0, n_components=100)),
                ("rbf2", RBFSampler(gamma=2.0, n_components=100)),
                ("rbf3", RBFSampler(gamma=1.0, n_components=100)),
                ("rbf4", RBFSampler(gamma=0.5, n_components=100))
                ])
        self.featurizer.fit(self.scaler.transform(observation_examples))

        self.models = []
        for _ in range(env.action_space.n):
            model = SGDRegressor(learning_rate="constant")
            # We need to call partial_fit once to initialize the model
            # or we get a NotFittedError when trying to make a prediction
            # This is quite hacky.
            model.partial_fit([self.featurize_state(env.reset())], [0])
            self.models.append(model)
        
    def featurize_state(self, state):
        """
        Returns the featurized representation for a state.
        """
        scaled = self.scaler.transform([state])
        featurized = self.featurizer.transform(scaled)
        return featurized[0]
    
    def predict(self, s, a=None):
        """
        Makes value function predictions.
        
        Args:
            s: state to make a prediction for
            a: (Optional) action to make a prediction for
            
        Returns
            If an action a is given this returns a single number as the prediction.
            If no action is given this returns a vector or predictions for all actions
            in the environment where pred[i] is the prediction for action i.
            
        """
        features = self.featurize_state(s)
        if not a:
            return np.array([m.predict([features])[0] for m in self.models])
        else:
            return self.models[a].predict([features])[0]
    
    def update(self, s, a, y):
        """
        Updates the estimator parameters for a given state and action towards
        the target y.
        """
        features = self.featurize_state(s)
        self.models[a].partial_fit([features], [y])
    
    def make_epsilon_greedy_policy(self, epsilon, nA):
        """
        Creates an epsilon-greedy policy based on a given Q-function approximator and epsilon.
        
        Args:
            estimator: An estimator that returns q values for a given state
            epsilon: The probability to select a random action . float between 0 and 1.
            nA: Number of actions in the environment.
        
        Returns:
            A function that takes the observation as an argument and returns
            the probabilities for each action in the form of a numpy array of length nA.
        
        """
        def policy_fn(observation):
            A = np.ones(nA, dtype=float) * epsilon / nA
            q_values = self.predict(observation)
            best_action = np.argmax(q_values)
            A[best_action] += (1.0 - epsilon)
            return A
        return policy_fn
    
    def get_value(self, observation, action: int):
        """
        """
        q_value = self.predict(observation, action)

        return q_value

    def train(self, bot, i_episode, discount_factor=1.0, epsilon=0.1, epsilon_decay=1.0, is_test=False):
        if is_test:
            assert epsilon == 0
      
        # The policy we're following
        policy = self.make_epsilon_greedy_policy(
            epsilon * epsilon_decay**i_episode, self.env.action_space.n)
        
        cum_reward = 0
        
        # Reset the environment and pick the first action
        state = self.env.reset()
        
        # Only used for SARSA, not Q-Learning
        next_action = None
        
        # One step in the environment
        for t in itertools.count():
                        
            # Choose an action to take
            if next_action is None:
                action_probs = policy(state)
                action = np.random.choice(np.arange(len(action_probs)), p=action_probs)
            else:
                action = next_action

            enemy_action = bot.get_action(state)
            
            # Take a step
            next_state, reward, done, _ = self.env.step(
                actions=(
                    action,
                    enemy_action
                )
            )
            
            if done:
                reward = -20
            
            # TD Update
            q_values_next = self.predict(next_state)
            
            # Use this code for Q-Learning
            # Q-Value TD Target
            td_target = reward + discount_factor * np.max(q_values_next)
            
            # Update the function approximator using our target if it's not a test
            if not is_test:
                self.update(state, action, td_target)

            cum_reward += reward
                
            if done or t == 100:
                break

            state = next_state
        
        return cum_reward, t
    
    def q_learning(self, num_episodes, discount_factor=1.0, epsilon=0.1, epsilon_decay=1.0, log_interval=100, test_iterations=100):
        """
        """
        
        bot = DeterministicFollowerAgent(
            world=self.env.world,
            me=self.env.world.player_2,
            enemy=self.env.world.player_1
        )
        
        rewards_return = []

        for i_episode in range(num_episodes):

            cum_reward, t = self.train(bot, i_episode, discount_factor=0.9, epsilon=1.0, epsilon_decay=0.999)

            # Logging
            if i_episode % log_interval == 0:
                mean_cum_reward = 0
                mean_t = 0
                for _ in range(test_iterations):
                    cum_reward, t = self.train(bot, i_episode, epsilon=0.0, is_test=True)
                    mean_cum_reward += cum_reward / test_iterations
                    mean_t += t / test_iterations

                with open('rewards.csv', 'a') as csv_file:
                    csv_file.write(str(i_episode) + "," + f"{mean_cum_reward}" + "," + str(mean_t) + "," + str(epsilon * epsilon_decay**i_episode) + "\n")
                    print('Eval rewards (epsilon:', epsilon * epsilon_decay**i_episode, ') episode', i_episode, ':', mean_cum_reward, "->", mean_t)

                rewards_return.append(mean_cum_reward)
                dump([self.scaler, self.featurizer, self.models], 'model.joblib')
    
        return rewards_return

def train():
    with open('rewards.csv', 'w') as csv_file:
        csv_file.write("epoch" + "," + "reward,"+ "steps,epsilon\n")

    env = RobotTagV2.from_grid(grid=ULTIMATE_GRID)
    env.reward_function = lambda world: 1

    agent = QLearningLFAAgent(env=env)
    return agent.q_learning(20000, discount_factor=0.9, epsilon=1.0, epsilon_decay=0.999)


if __name__ == '__main__':
    train()
