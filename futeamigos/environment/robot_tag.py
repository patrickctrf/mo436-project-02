from typing import Callable, List, Tuple

from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.reward_functions import euclidean_distance
from futeamigos.environment.world import World
from gym import Env, spaces

import numpy as np


class RobotTagEnv(Env):
    """
    Gym Environment for the RobotTag (futeamigos) task

    To build an Env from a predefined grid, use `from_grid` constructor.

    Args:
        world: an World instance defining the grid
    """

    def __init__(self, world: World):
        self.world = world

        self.action_space = spaces.Discrete(9)
        self.observation_space = spaces.Box(np.array([0, 0, 0, 0]), np.array([16, 9, 16, 9]), dtype=np.float32)

        self.reward_function: Callable[[World], float] = euclidean_distance

    def step(self, actions: Tuple[int, int]) \
            -> Tuple[CharacterObservation, float, bool, str]:
        """
        Makes a step in the env, moving both characters

        Args:
            actions: a tuple of two integers, each one defining the direction
                to each character will move

        Returns:
            A tuple with both characters observation concatenated, the reward,
                the done signal and an info signal
        """
        player_1 = self.world.player_1
        player_2 = self.world.player_2

        player_1.step(direction=actions[0])
        player_2.step(direction=actions[1])

        done = player_1.touched(other=player_2)

        reward = self.reward_function(world=self.world)

        return (*player_1.state(), *player_2.state()), reward, done, ''

    def reset(self):
        """
        Reset the env state to a random state
        """
        self.world.random_init()

        player_1 = self.world.player_1
        player_2 = self.world.player_2

        return (*player_1.state(), *player_2.state())

    def render(self):
        """
        Prints the grid (including the players current position)
        """
        self.world.render()

    @classmethod
    def from_grid(cls,  grid: List[List[str]]) -> 'RobotTagEnv':
        """
        Creates the env from a predefined grid. See the module
        `futeamigos.grids` for examples

        Args:
            grid: the grid, a List of strings containing `W` characters to
                demark the walls
        """
        world = World.from_grid(grid=grid)

        return RobotTagEnv(world=world)


if __name__ == '__main__':
    import time
    from os import system

    from futeamigos.environment import (BASIC_GRID, CardinalDirections,
                                        DeterministicFollowerAgent)

    env = RobotTagEnv.from_grid(grid=BASIC_GRID)

    follower_agent = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    for _ in range(10):
        observation = env.reset()
        done = False

        while not done:
            system('clear')
            env.render()
            time.sleep(0.5)

            observation, reward, done, world = env.step(
                actions=(
                    CardinalDirections.CENTER,
                    follower_agent.get_action(observation=observation)
                )
            )
