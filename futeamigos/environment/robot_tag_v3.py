import numpy as np
from futeamigos.environment.mutating_world import Grids, MutatingWorld
from futeamigos.environment.robot_tag_v2 import RobotTagV2


class Slices:
    """
    Defines slices for each type of entity in the grid
    """

    WALL_SLICE = 0
    PLAYER_1_SLICE = 1
    PLAYER_2_SLICE = 2


class RobotTagV3(RobotTagV2):
    """
    Gym Environment for the RobotTag (futeamigos) task
    This version observe the entities positions as slices of an array

    To build an Env from a list of predefined grids, use `from_grids`
    constructor.

    Args:
        grids: a list of grids
    """

    def observe(self) -> np.array:
        """
        Returns the observation as an NumPy Array, where each slice represents
        the presence of each of the instances
        """
        height = self.world.height
        width = self.world.width
        player_1 = self.world.player_1
        player_2 = self.world.player_2
        walls = self.world.walls

        grid = np.zeros((3, height, width))

        for wall in walls:
            grid[Slices.WALL_SLICE][wall.y][wall.x] = 1

        grid[Slices.PLAYER_1_SLICE][player_1.y][player_1.x] = 1
        grid[Slices.PLAYER_2_SLICE][player_2.y][player_2.x] = 1

        return np.array(grid)

    @classmethod
    def from_grids(cls, grids: Grids, keep: float = 0.7) -> 'RobotTagV3':
        """
        Creates the env from a list of predefined grids. See the module
        `futeamigos.grids` for examples

        Args:
            grids: a list of grids. Each grid is a List of strings containing
                `W` characters to demark the walls
            keep: how many of the original walls should be kept
        """
        world = MutatingWorld.from_grids(grids=grids, keep=keep)

        return RobotTagV3(world=world)
