"""
This module defines basic grids for simple experiments
"""

BASIC_GRID = [
    "          ",
    "          ",
    "          ",
    "          ",
    "          ",
    "          ",
    "          ",
    "          ",
    "          ",
    "          ",
]

ULTIMATE_GRID = [
    "W WW   W  ",
    "    W     ",
    "    W  W W",
    "WW    WW  ",
    "    W    W",
    "WW  W    W",
    "         W",
    "W   WWWW  ",
    "WWW   WW  ",
    "W     WW  ",
]

HORIZONTAL_GRID = [
    "          ",
    "WWWWW WW W",
    "          ",
    "W WWWW WW ",
    "          ",
    "W WW WWWW ",
    "          ",
    "WWW  WWW W",
    "          ",
    "  WWWWW   ",
]

VERTICAL_GRID = [
    "         W",
    "   W  W   ",
    "W  W     W",
    "      W   ",
    "W  W  W  W",
    " W W   W  ",
    "   W     W",
    " W     W  ",
    " W W   W  ",
    " W     W  ",
]

ROOMS_GRID = [
    "          ",
    "   W   W  ",
    "WWWW   WWW",
    "     W    ",
    "WWW    WWW",
    "  W    W  ",
    "       W  ",
    "  W       ",
    "  W    W  ",
    "WWW    WWW",
]

CENTER_ROOM_GRID = [
    "          ",
    "          ",
    "  WW WWW  ",
    "  W    W  ",
    "    W  W  ",
    "  W W  W  ",
    "  W    W  ",
    "  WW WWW  ",
    "          ",
    "          ",
]

POINT_GRID = [
    "   W   W W",
    " W   W W  ",
    "   W   W W",
    " W   W    ",
    "   W   W W",
    " W   W W  ",
    "   W     W",
    " W   W W  ",
    "   W   W W",
    " W   W W  ",
]

POINT2_GRID = [
    "  W       ",
    "W WWWWW WW",
    "    W     ",
    "W WWWWW WW",
    "    W     ",
    "W WWWWW WW",
    "    W     ",
    "W WWWW WWW",
    "    W     ",
    " W  W  W  ",
]

GRID_ROTATION = [
    ULTIMATE_GRID,
    HORIZONTAL_GRID,
    VERTICAL_GRID,
    ROOMS_GRID,
    CENTER_ROOM_GRID,
    POINT2_GRID
]
