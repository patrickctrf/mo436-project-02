from random import choice
from typing import List

from futeamigos.environment.world import World, Character

Grids = List[List[List[str]]]


class MutatingWorld(World):
    """
    Similar to World, but uses a rotation of grids instead of a fixed grid

    Args:
        grids: a list of grids that will be used in random_init
        keep: how many of the original walls should be kept
    """

    def __init__(self, height: int, width: int, grids: Grids, keep: float):
        self.grids = grids

        self.height = height
        self.width = width

        self.player_1 = Character(x=0, y=0, world=self)
        self.player_2 = Character(x=0, y=0, world=self)

        self.keep = keep

    def random_init(self):
        """
        Put the player characters in random positions
        """
        world = self.from_grid(grid=choice(self.grids))
        world.dissolve_walls(keep=self.keep)
        world.random_init()

        self.player_1.x = world.player_1.x
        self.player_1.y = world.player_1.y

        self.player_2.x = world.player_2.x
        self.player_2.y = world.player_2.y

        self.walls = world.walls

    @classmethod
    def from_grids(cls, grids: Grids, keep: float) -> 'MutatingWorld':
        """
        Creates the MutatingWorld from a list of predefined grids. See the
        module `futeamigos.grids` for examples. All grids should have the same
        dimensions

        Args:
            grids: a list of grids. Each grid is a List of strings containing
                `W` characters to demark the walls
            keep: how many of the original walls should be kept
        """

        return MutatingWorld(
            height=len(grids[0]),
            width=len(grids[0][0]),
            grids=grids,
            keep=keep
        )
