from math import ceil, floor
from random import choice, sample
from typing import List, Tuple

from futeamigos.environment.character import Character, Entity
from futeamigos.environment.directions import CardinalDirections

Grid = List[List[int]]


class Wall(Entity):
    """
    Represents an Wall in the grid

    Args:
        x: the x position in the gridworld
        y: the y position in the gridworld
    """

    def __init__(self, x: int, y: int):
        super().__init__(x=x, y=y)


def replace_in_string(line: str, pos: int, char: str):
    """
    Replaces a character in a string

    Args:
        line: the original string
        pos: the postion where the char should be placed
        char: the char that will be placed
    """
    return line[:pos] + char + line[pos+1:]


def render_entity(entity: Entity, grid: Grid, char: str):
    """
    Renders an Entity in a grid

    Args:
        entity: the entity that will be rendered
        grid: the grid
        char: the char representing the entity
    """
    new_line = replace_in_string(
        line=grid[entity.y],
        pos=entity.x,
        char=char
    )

    grid[entity.y] = new_line

    return grid


def get_avaiable_positions(grid: Grid, width: int, to_left: bool = True) \
        -> List[Tuple[int, int]]:
    """
    Returns a list of free positions where an Entity can be placed

    Args:
        grid: the grid representation
        width: the grid width
        to_left: wether to use only the positions to the leftmost or rightmost
            positions of the grid. This way characters will not begins in a
            close position
    """
    avaiable_positions = []
    # Sample a position to player_1
    for y, line in enumerate(grid):
        # TODO: this is poorly implemented
        if to_left:
            x = 0
            while x < floor(width/2):
                if line[x] != 'W':
                    avaiable_positions.append((x, y))

                x += 1

        else:
            x = ceil(width/2)
            while x < width:
                if line[x] != 'W':
                    avaiable_positions.append((x, y))

                x += 1

    return avaiable_positions


class World:
    """
    This class manages the grid and the characters interaction with it

    Args:
        height: the grid height
        width: the grid width
    """

    def __init__(self, height: int, width: int, walls: List[Wall]):
        self.height = height
        self.width = width

        self.walls = walls

        self.player_1 = Character(x=1, y=1, world=self)
        self.player_2 = Character(x=2, y=2, world=self)

    def random_init(self):
        """
        Put the player characters in random positions
        """
        sides = choice([[True, False], [False, True]])

        for player, side in zip([self.player_1, self.player_2], sides):
            grid = self.render_grid()

            positions = get_avaiable_positions(
                grid=grid,
                width=self.width,
                to_left=side
            )

            player_position = choice(positions)

            player.x = player_position[0]
            player.y = player_position[1]

    def can_walk(self, entity: Character, direction: int) -> bool:
        """
        Verifies if an entity can move in the given direction

        Args:
            entity: the entity that will be moved
            direction: the direction where the character will move to, as
                defined in `CardinalDirections`

        Returns:
            True if the entity can move to desired position and False otherwise
        """
        if direction == CardinalDirections.CENTER:
            return True

        deslocation = CardinalDirections.DESLOC_TO_VECTOR[direction]

        n_position = (
            entity.x + deslocation[0],
            entity.y + deslocation[1]
        )

        # The Character is not allowed to move away from the grid horizontally
        if n_position[0] < 0 or n_position[0] >= self.width:
            return False

        # The Character is not allowed to move away from the grid horizontally
        if n_position[1] < 0 or n_position[1] >= self.height:
            return False

        for entity in self.walls + [self.player_1, self.player_2]:
            if entity.x == n_position[0] and entity.y == n_position[1]:
                return False

        return True

    def render(self):
        """
        Prints the grid (including the players current position)
        """
        grid = self.render_grid()

        for line in grid:
            print(end='|')
            for collumn in line:
                print(collumn, end='')

            print('|')

    def render_grid(self, render_players: bool = True):
        """
        Returns the grid

        Args:
            render_players: if True, will renders player 1 and 2 as A and B in
                the grid
        """
        grid = [self.width * ' ' for _ in range(self.height)]

        for wall in self.walls:
            render_entity(entity=wall, grid=grid, char='W')

        if render_players:
            render_entity(entity=self.player_1, grid=grid, char='A')
            render_entity(entity=self.player_2, grid=grid, char='B')

        return grid

    @classmethod
    def from_grid(cls,  grid: List[str]) -> 'World':
        """
        Creates the env from a predefined grid. See the module
        `futeamigos.grids` for examples

        Args:
            grid: the grid, a List of strings containing `W` characters to
                demark the walls
        """
        walls = []

        for y, line in enumerate(grid):
            for x, cell in enumerate(line):
                if cell == 'W':
                    walls.append(
                        Wall(x=x, y=y)
                    )

        return World(
            height=len(grid),
            width=len(grid[0]),
            walls=walls
        )

    def dissolve_walls(self, keep: float):
        """
        Randomly removes some of the walls to randomize initialization

        Args:
            keep: how many of the walls should be kept
        """
        self.walls = sample(self.walls, int(keep * len(self.walls)))
