from time import sleep
from typing import Tuple

from gym import spaces
from gym.spaces import space
from numpy import inf, moveaxis

from futeamigos.environment.mutating_world import Grids, MutatingWorld
from futeamigos.environment.robot_tag_v2 import SimplifiedObservation
from futeamigos.environment.robot_tag_v3 import RobotTagV3


class Slices:
    """
    Defines slices for each type of entity in the grid
    """

    WALL_SLICE = 0
    PLAYER_1_SLICE = 1
    PLAYER_2_SLICE = 2


class RobotTagV4(RobotTagV3):
    """
    Gym Environment for the RobotTag (futeamigos) task
    This version observe the entities positions as slices of an array

    To build an Env from a list of predefined grids, use `from_grids`
    constructor.

    Args:
        grids: a list of grids
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.max_episodes_timesteps = 200

        from futeamigos.environment import DeterministicFollowerAgent

        self.follower_agent = DeterministicFollowerAgent(
            world=self.world,
            me=self.world.player_2,
            enemy=self.world.player_1
        )

        self.action_space = spaces.Discrete(9)
        self.observation_space = spaces.Box(shape=(10, 10, 3), low=-inf, high=+inf)

    def max_episode_timesteps(self):
        return self.max_episodes_timesteps

    def step(self, actions, *args, **kwargs) -> Tuple[SimplifiedObservation, float, bool, str]:
        # self.render()
        # sleep(0.5)
        enemy_action = self.follower_agent.get_action(self.observe()[:-1])

        return super().step(*args, actions=(actions, enemy_action), **kwargs)

    def states(self):
        return dict(type="float", shape=(10, 10, 3))

    def actions(self):
        return dict(type="int", num_values=9)

    def observe(self):
        return moveaxis(super().observe(), 0, 2)

    @classmethod
    def from_grids(cls, grids: Grids, keep: float = 0.7) -> 'RobotTagV3':
        """
        Creates the env from a list of predefined grids. See the module
        `futeamigos.grids` for examples

        Args:
            grids: a list of grids. Each grid is a List of strings containing
                `W` characters to demark the walls
            keep: how many of the original walls should be kept
        """
        world = MutatingWorld.from_grids(grids=grids, keep=keep)

        return RobotTagV4(world=world)
