class CardinalDirections:
    """
    Defines direction to which a character can move
    """
    NORTHWEST = 0
    NORTH = 1
    NORTHEAST = 2
    EAST = 3
    SOUTHEAST = 4
    SOUTH = 5
    SOUTHWEST = 6
    WEST = 7
    CENTER = 8

    VECTOR_TO_DESLOC = {
        (-1, -1): NORTHWEST,
        (0, -1): NORTH,
        (1, -1): NORTHEAST,
        (1, 0): EAST,
        (1, 1): SOUTHEAST,
        (0, 1): SOUTH,
        (-1, 1): SOUTHWEST,
        (-1, 0): WEST,
        (0, 0): CENTER
    }

    DESLOC_TO_VECTOR = {
        value: key
        for key, value in VECTOR_TO_DESLOC.items()
    }

    @classmethod
    def __iter__(cls):
        """
        """
        return iter([
            cls.NORTHWEST,
            cls.NORTH,
            cls.NORTHEAST,
            cls.WEST,
            cls.CENTER,
            cls.EAST,
            cls.SOUTHWEST,
            cls.SOUTH,
            cls.SOUTHEAST,
        ])
