from typing import Tuple

import numpy as np
from futeamigos.environment.directions import CardinalDirections

CharacterObservation = Tuple[int, int, int]


class Entity:
    """
    The representation of a tangible object in the Gridworld

    Args:
        x: the x position in the gridworld
        y: the y position in the gridworld
    """

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def position(self) -> Tuple[int, int]:
        """
        Returns a tuple (x, y) with the current character position
        """
        return (self.x, self.y)

    def __repr__(self):
        return f"Entity(x={self.x}, y={self.y})"


class Character(Entity):
    """
    Represents a mobile Entity in the gridworld

    Args:
        x: the initial x position of the character in the gridworld
        y: the initial y position of the character in the gridworld
        color: a string representing the character color in the gridworld
    """

    def __init__(self, x: int, y: int, world: 'World'):
        super().__init__(x=x, y=y)

        self.x = x
        self.y = y

        self.last_x = x
        self.last_y = y

        self.world = world

        self.last_direction = CardinalDirections.CENTER

    def desloc(self) -> Tuple[int, int]:
        """
        Returns the deslocation vector since the last step
        """
        return (
            self.x - self.last_x,
            self.y - self.last_y
        )

    def state(self) -> Tuple[int, int, int]:
        """
        Returns the Character state

        Returns:
            Returns a tuple with characters x, y and last direction
        """
        return self.x, self.y, self.last_direction

    def step(self, direction: int):
        """
        Makes the Character move to the desired direction (if there is no
        Entity blocking)

        Args:
            direction: the direction where the character will move to, as
                defined in `CardinalDirections`
        """
        self.last_x = self.x
        self.last_y = self.y

        deslocation = CardinalDirections.DESLOC_TO_VECTOR[direction]

        if self.world.can_walk(entity=self, direction=direction):
            self.x = deslocation[0] + self.x
            self.y = deslocation[1] + self.y

            self.last_direction = direction

        else:
            self.last_direction = CardinalDirections.CENTER

    def touched(self, other: Entity) -> bool:
        """
        Returns True if this entity has touched another entity

        Args:
            other: the other entity
        """
        desloc = np.array(self.position()) - np.array(other.position())

        return np.linalg.norm(desloc) < 1.42

    def __repr__(self):
        return (f"Character(x={self.x}, "
                f"y={self.y}, "
                f"last_direction={self.last_direction})")
