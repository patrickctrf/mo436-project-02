from queue import Queue
from threading import Thread

import torch
from torch.utils.data import Dataset


class DataManager(Thread):
    def __init__(self, data_loader, buffer_size=3, device=torch.device("cpu"), data_type=torch.float32):
        """
This manager intends to load a PyTorch dataloader like from disk into memory,
reducing the acess time. It does not easily overflow memory, because we set a
buffer size limiting how many samples will be loaded at once. Everytime a sample
is consumed by the calling thread, another one is replaced in the
buffer (unless we reach the end of dataloader).

A manger may be called exactly like a dataloader, an it's based in an internal
thread that loads samples into memory in parallel. This is specially useful
when you are training in GPU and processor is almost idle.

        :param data_loader: Base dataloader to load in parallel.
        :param buffer_size: How many samples to keep loaded (caution to not overflow RAM). Default: 3.
        :param device: Torch device to put samples in, like torch.device("cpu") (default). It saves time by transfering in parallel.
        :param data_type: Automatically casts tensor type. Default: torch.float32.
        """
        super().__init__()
        self.buffer_queue = Queue(maxsize=buffer_size)
        self.data_loader = data_loader
        self.buffer_size = buffer_size
        self.device = device
        self.data_type = data_type

        self.dataloader_finished = False

    def run(self):
        """
Runs the internal thread that iterates over the dataloader until fulfilling the
buffer or the end of samples.
        """
        for i, (x, y) in enumerate(self.data_loader):
            # Important to set before put in queue to avoid race condition
            # would happen if trying to get() in next() method before setting this flag
            if i >= len(self) - 1:
                self.dataloader_finished = True

            self.buffer_queue.put([x.to(self.data_type).to(self.device),
                                   y.to(self.data_type).to(self.device)])

    def __iter__(self):
        """
Returns an iterable of itself.

        :return: Iterable around this class.
        """
        self.start()
        self.dataloader_finished = False
        return self

    def __next__(self):
        """
Intended to be used as iterator.

        :return: Next iteration element.
        """
        if self.dataloader_finished is True and self.buffer_queue.empty():
            raise StopIteration()

        return self.buffer_queue.get()

    def __len__(self):
        return len(self.data_loader)


class BufferDataset(Dataset):
    def __init__(self, buffer):
        self.buffer = buffer
        self.length = len(buffer)
        return

    def __getitem__(self, idx):
        return self.buffer[idx]

    def __len__(self):
        return self.length
