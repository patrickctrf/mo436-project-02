import torch
from numpy import zeros, where, hstack, array, arange, argmax, delete, append, nonzero
from numpy.random import choice
from torch import nn, randn, as_tensor
from torch.nn import MSELoss
from torch.utils.data import DataLoader
from tqdm import tqdm

from futeamigos.environment import BASIC_GRID, CardinalDirections
from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.world import World
from futeamigos.offpolicy.dataclasses import DataManager, BufferDataset

if torch.cuda.is_available():
    dev = "cuda:0"
    print("Usando GPU")
else:
    dev = "cpu"
    print("Usando CPU")
device = torch.device(dev)


def validate(env, my_agent, follower_agent):
    total_steps = 0
    total_episodes = 20
    for episode in range(total_episodes):
        observation = env.reset()
        done = False
        for n_steps in range(100):
            if done is True:
                break

            # # Clear and print the grid in stdout
            # system('clear')
            # env.render()
            # sleep(0.3)

            # get_action should return an int
            my_action = my_agent.get_action(
                observation=observation
            )

            enemy_action = follower_agent.get_action(observation)

            observation, reward, done, _ = env.step(
                actions=(
                    my_action,
                    enemy_action
                )
            )

            if env.world.player_1.touched(other=env.world.player_2):
                done = True

        total_steps += n_steps

        # Return mean steps
        return total_steps / (episode + 1)

        # print("\n\n\n\n\nn_steps:", n_steps)
        # sleep(5)


class DqnAgent(nn.Module):
    def __init__(self, env):
        super().__init__()
        self.env = env

        # Basic structure for our model
        self.features_block = nn.Sequential(nn.Conv2d(3, 100, kernel_size=(5, 5)),
                                            nn.Conv2d(100, 30, kernel_size=(3, 3)),
                                            nn.Conv2d(30, 30, kernel_size=(3, 3)))
        # Checking features dimension
        features_example = self.features_block(randn((1, 3, 10, 10)))
        self.fc_block = \
            nn.Sequential(nn.Flatten(),
                          nn.Linear(features_example.flatten().shape[0], 120),
                          nn.ReLU(),
                          nn.Linear(120, 100), nn.ReLU(),
                          nn.Linear(100, 9))

    def forward(self, x):
        x = self.features_block(x)
        x = self.fc_block(x)

        return x

    def get_action(self, observation: CharacterObservation, deterministic: bool = True) -> int:
        """
Get action provided by agent's policy at current trained state.

        :param observation: Environment observation.
        :type observation: CharacterObservation
        :param deterministic: Whether actuate in training (False) or test (True, default).
        :type deterministic: bool
        :return: Which action to take (cardinal direction from 0 to 8).
        :rtype: int
        """
        q_values = self.predict(observation)
        best_action = argmax(q_values.detach().numpy())

        return best_action

    def get_value(self, observation: CharacterObservation, action: int) -> float:
        """
Get action-value provided by a given action at specific observation (state s).

        :param observation: Environment observation.
        :type observation: CharacterObservation
        :param action: Action to evaluate (from 0 to 8).
        :type action: int
        :return: Which action to take (cardinal direction from 0 to 8).
        :rtype: int
        """
        return self.predict(observation, action)

    def predict(self, s, a=None):
        """
        Makes value function (Q(s,a)) predictions.

        Args:
            s: state to make a prediction for
            a: (Optional) action to make a prediction for

        Returns
            If an action a is given this returns a single number as the prediction.
            If no action is given this returns a vector or predictions for all actions
            in the environment where pred[i] is the prediction for action i.

        """
        if a is None:
            return self(s)
        else:
            return self(s)[a]


def custom_reward(world: World) -> float:
    """
Defining our custom reward according to our objective.

    :param world: World with environment information.
    :type world: World
    :return: Reward for current Q.
    :rtype: float
    """
    if world.player_1.touched(other=world.player_2):
        # We got caught
        return -15.0
    else:
        # We are still running away
        return 1.0


def update_network(my_agent, buffer, gamma=0.9):
    loss_function = MSELoss

    optimizer = torch.optim.Adam(my_agent.parameters(), lr=1e-3)

    dataset = BufferDataset(buffer)

    dataloader = DataLoader(dataset, batch_size=64, shuffle=True)

    train_manager = DataManager(dataloader, device=device, buffer_size=3)

    optimizer.zero_grad()
    for j, (s, a, r, s_) in enumerate(train_manager):
        q = my_agent.get_value(s, a)

        a_ = my_agent.get_value(s_)

        q_ = my_agent.get_value(s_, a_)

        # Repare que NAO ESTAMOS acumulando a LOSS.
        single_loss = loss_function(, y.view(-1, self.output_features))
        # Cada chamada ao backprop eh ACUMULADA no gradiente (optimizer)
        single_loss.backward()

        # Otimizamos em batch
        optimizer.step()
        optimizer.zero_grad()


def save_on_buffer(circular_buffer, params, max_buffer_size=1e6):
    s, a, r, s_ = params

    if a is None or r is None:
        return 0

    circular_buffer.append((s, a, r, s_))

    if len(circular_buffer) > max_buffer_size:
        circular_buffer.pop(0)

    return 0


def train(env, my_agent, follower_agent):
    # This array checks the mean of last 5 validations and, when converged, ends the train
    convergence_circular_buffer = zeros((5,))

    # s a r s
    experience_replay_buffer = []

    with open("n_steps.csv", mode="w") as log_file, open("validation_steps.csv", mode="w") as val_log_file:
        tqdm_bar = tqdm(range(15000))
        for episode in tqdm_bar:

            observation = None
            my_action = None
            reward = None

            observation = env.reset()
            done = False

            gamma = 0.9

            for n_steps in range(100):
                if done is True:
                    break

                # # Clear and print the grid in stdout
                # system('clear')
                # env.render()

                observation_old = observation.copy()
                my_action_old = my_action
                reward_old = reward

                # get_action should return an int
                my_action = my_agent.get_action(
                    observation=as_tensor(observation, dtype=torch.float32).view(1, 3, 10, 10)
                )

                enemy_action = follower_agent.get_action(observation)

                observation, reward, done, _ = env.step(
                    actions=(
                        my_action,
                        enemy_action
                    )
                )

                if env.world.player_1.touched(other=env.world.player_2):
                    done = True

                save_on_buffer(experience_replay_buffer, (observation_old, my_action_old, reward_old, observation))

                if n_steps % 10:
                    update_network(my_agent)

                # at end of episode
                update_network(my_agent)

            # Compute the mean steps for episode
            mean_steps /= batch_size
            log_file.write(str(mean_steps + 1) + "\n")
            log_file.flush()
            tqdm_bar.set_description("Episodio: " + str(episode) + ". Mean_steps: " + str(mean_steps))

            # Validation each 10 epochs, and update params!
            if episode % 10 == 0:
                validated = validate(env, my_agent, follower_agent)
                val_log_file.write(str(validated) + "\n")
                val_log_file.flush()
                convergence_circular_buffer = delete(convergence_circular_buffer, 0)
                convergence_circular_buffer = append(convergence_circular_buffer, validated)

                if convergence_circular_buffer.mean() >= 99:
                    print("Training process converged.")
                    # Our model is trained

                    with open('validation_steps.csv') as f:
                        lines = f.readlines()
                        x = [line.split()[0] for line in lines]

                    x = array(x).astype("float32")
                    return x

    with open('validation_steps.csv') as f:
        lines = f.readlines()
        x = [line.split()[0] for line in lines]

    print("Model did not fully converge")
    x = array(x).astype("float32")
    return x
