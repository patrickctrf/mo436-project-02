from matplotlib import pyplot as plt
from numpy import array

with open('validation_steps.csv') as f:
    lines = f.readlines()
    x = [line.split()[0] for line in lines]
    
x = array(x).astype("float32")
    
plt.close()
plt.plot(x)
# plt.show()
plt.savefig("grafico_validacao_reward.png")

with open('n_steps.csv') as f:
    lines = f.readlines()
    x = [line.split()[0] for line in lines]
    
x = array(x).astype("float32")

plt.close()
plt.plot(x)
# plt.show()
plt.savefig("grafico_busca_reward.png")
