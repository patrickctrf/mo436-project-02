import pickle

import sklearn.pipeline
from numpy import zeros, where, hstack, array, arange, clip, argmax, nonzero, delete, append
from numpy.random import choice
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDRegressor
from tqdm import tqdm

from futeamigos.environment import DeterministicFollowerAgent, CardinalDirections
from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.grids import ULTIMATE_GRID, BASIC_GRID
from futeamigos.environment.robot_tag_v2 import RobotTagV2
from futeamigos.environment.world import World


def validate(env, my_agent, follower_agent):
    total_steps = 0
    total_episodes = 20
    for episode in range(total_episodes):
        observation = env.reset()
        done = False
        for n_steps in range(100):
            if done is True:
                break

            # # Clear and print the grid in stdout
            # system('clear')
            # env.render()
            # sleep(0.3)

            # get_action should return an int
            my_action = my_agent.get_action(
                observation=observation
            )

            enemy_action = follower_agent.get_action(observation)

            observation, reward, done, _ = env.step(
                actions=(
                    my_action,
                    enemy_action
                )
            )

            if env.world.player_1.touched(other=env.world.player_2):
                done = True

        total_steps += n_steps

        # Return mean steps
        return total_steps / (episode + 1)

        # print("\n\n\n\n\nn_steps:", n_steps)
        # sleep(5)


class MonteCarloAgent(object):
    def __init__(self, env):
        self.env = env

        observation_examples = array([clip(env.observation_space.sample(), -20, 20) for x in range(10000)])
        self.scaler = sklearn.preprocessing.StandardScaler()
        self.scaler.fit(observation_examples)

        # Used to convert a state to a featurizes represenation.
        # We use RBF kernels with different variances to cover different parts of the space
        self.featurizer = sklearn.pipeline.FeatureUnion([
            ("rbf1", RBFSampler(gamma=5.0, n_components=100)),
            ("rbf2", RBFSampler(gamma=2.0, n_components=100)),
            ("rbf3", RBFSampler(gamma=1.0, n_components=100)),
            ("rbf4", RBFSampler(gamma=0.5, n_components=100))
        ])
        self.featurizer.fit(self.scaler.transform(observation_examples))

        self.lfas = []
        for _ in range(env.action_space.n):
            model = SGDRegressor(learning_rate="constant")
            # We need to call partial_fit once to initialize the model
            # or we get a NotFittedError when trying to make a prediction
            # This is quite hacky.
            model.partial_fit([self.featurize_state(env.reset())], [0])
            self.lfas.append(model)

    def get_action(self, observation: CharacterObservation, deterministic: bool = True) -> int:
        """
Get action provided by agent's policy at current trained state.

        :param observation: Environment observation.
        :type observation: CharacterObservation
        :param deterministic: Whether actuate in training (False) or test (True, default).
        :type deterministic: bool
        :return: Which action to take (cardinal direction from 0 to 8).
        :rtype: int
        """
        q_values = self.predict(observation)
        best_action = argmax(q_values)

        return best_action

    def get_value(self, observation: CharacterObservation, action: int) -> float:
        """
Get action-value provided by a given action at specific observation (state s).

        :param observation: Environment observation.
        :type observation: CharacterObservation
        :param action: Action to evaluate (from 0 to 8).
        :type action: int
        :return: Which action to take (cardinal direction from 0 to 8).
        :rtype: int
        """
        return self.predict(observation, action)

    def featurize_state(self, state):
        """
Returns the featurized representation for a state. (400 from RBFSampler)
        """
        scaled = self.scaler.transform([state])
        featurized = self.featurizer.transform(scaled)
        return featurized[0]

    def predict(self, s, a=None):
        """
        Makes value function (Q(s,a)) predictions.

        Args:
            s: state to make a prediction for
            a: (Optional) action to make a prediction for

        Returns
            If an action a is given this returns a single number as the prediction.
            If no action is given this returns a vector or predictions for all actions
            in the environment where pred[i] is the prediction for action i.

        """
        features = self.featurize_state(s)
        if a is None:
            return array([lfa.predict([features])[0] for lfa in self.lfas])
        else:
            return self.lfas[a].predict([features])[0]

    def update(self, s, a, y):
        """
        Updates the estimator parameters for a given state and action towards
        the target y.
        """
        features = self.featurize_state(s)
        self.lfas[a].partial_fit([features], [y])


def custom_reward(world: World) -> float:
    """
Defining our custom reward according to our objective.

    :param world: World with environment information.
    :type world: World
    :return: Reward for current Q.
    :rtype: float
    """
    if world.player_1.touched(other=world.player_2):
        # We got caught
        return -15.0
    else:
        # We are still running away
        return 1.0


def train(env, my_agent, follower_agent):
    # This array checks the mean of last 5 validations and, when converged, ends the train
    convergence_circular_buffer = zeros((5,))
    with open("n_steps.csv", mode="w") as log_file, open("validation_steps.csv", mode="w") as val_log_file:
        q_permanent_counter = zeros(2 * (len(BASIC_GRID[0]), len(BASIC_GRID),) + (len(CardinalDirections.DESLOC_TO_VECTOR.keys()),))
        tqdm_bar = tqdm(range(15000))
        for episode in tqdm_bar:
            observation = env.reset()
            done = False
            q_aux_rewards = zeros(q_permanent_counter.shape)
            q_denominator_mean_array = zeros(q_permanent_counter.shape)

            gamma = 0.9

            # O perseguidor pode ser deterministico, mas a recompensa do estado
            # atual depende das futuras acoes, que sao random com epsilon chances,
            # entao a recompensa de cada estado pode mudar de acordo com o futuro,
            # pois o monte carlo calcula isto de acordo com o episodio inteiro.
            batch_size = 3
            mean_steps = 0.0
            for n_batches in range(batch_size):
                observation = env.reset()
                done = False
                q_first_visit_occurred = zeros(q_permanent_counter.shape)
                q_reward_gamma_discounter = zeros(q_permanent_counter.shape)

                for n_steps in range(100):
                    if done is True:
                        break

                    # # Clear and print the grid in stdout
                    # system('clear')
                    # env.render()

                    # get_action should return an int
                    my_action = my_agent.get_action(
                        observation=observation
                    )

                    epsilon = 3 / (3 + q_permanent_counter[tuple(observation)].sum())

                    # Epsilon greed policy chooses between random action and best agent action
                    my_action = choice(hstack((array(my_action), arange(9))),
                                       p=[1 - epsilon, epsilon / 9, epsilon / 9, epsilon / 9, epsilon / 9, epsilon / 9, epsilon / 9, epsilon / 9, epsilon / 9, epsilon / 9])

                    enemy_action = follower_agent.get_action(observation)

                    observation_old = list(observation)

                    observation, reward, done, _ = env.step(
                        actions=(
                            my_action,
                            enemy_action
                        )
                    )

                    if env.world.player_1.touched(other=env.world.player_2):
                        done = True

                    # First visit marked up!
                    q_first_visit_occurred[tuple(observation_old) + (my_action,)] = 1.0

                    q_reward_gamma_discounter *= gamma
                    q_reward_gamma_discounter[tuple(observation_old) + (my_action,)] = 1.0

                    # All alread visited states accumulate reward
                    q_aux_rewards = q_aux_rewards + reward * q_reward_gamma_discounter

                # We need to take the mean for each VISITED state
                q_denominator_mean_array += q_first_visit_occurred.copy()

                # We calculate the mean step our model achieved each episode
                mean_steps += n_steps

            q_permanent_counter += where(q_denominator_mean_array > 0, 1, 0)

            # Compute the mean steps for episode
            mean_steps /= batch_size
            log_file.write(str(mean_steps + 1) + "\n")
            log_file.flush()
            tqdm_bar.set_description("Episodio: " + str(episode) + ". Mean_steps: " + str(mean_steps))

            indices_to_update = nonzero(q_aux_rewards)

            for s in list(zip(*indices_to_update)):
                my_agent.update(s[:-1], s[-1], q_aux_rewards[s])

            # Validation each 10 epochs
            if episode % 10 == 0:
                validated = validate(env, my_agent, follower_agent)
                val_log_file.write(str(validated) + "\n")
                val_log_file.flush()
                convergence_circular_buffer = delete(convergence_circular_buffer, 0)
                convergence_circular_buffer = append(convergence_circular_buffer, validated)

                if convergence_circular_buffer.mean() >= 99:
                    print("Training process converged.")
                    # Our model is trained

                    with open('validation_steps.csv') as f:
                        lines = f.readlines()
                        x = [line.split()[0] for line in lines]

                    x = array(x).astype("float32")
                    return x

    with open('validation_steps.csv') as f:
        lines = f.readlines()
        x = [line.split()[0] for line in lines]

    print("Model did not fully converge")
    x = array(x).astype("float32")
    return x


if __name__ == '__main__':
    env = RobotTagV2.from_grid(grid=ULTIMATE_GRID)
    env.reward_function = custom_reward

    follower_agent = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    my_agent = MonteCarloAgent(env)

    training_results = train(env, my_agent, follower_agent)

    pickle.dump(my_agent, open("my_agent.sklearn", 'wb'))

    print("Rewards:", training_results)

# def update(self, s, a, y):
#     """
#     Updates the estimator parameters for a given state and action towards
#     the target y.
#
#     :param s: Lista de estados
#     :param a: a action que estamos avaliando
#     :param y: lista de saídas esperadas
#     """
#     features = [self.featurize_state(i) for i in s]
#     self.lfas[a].partial_fit(features, y)
