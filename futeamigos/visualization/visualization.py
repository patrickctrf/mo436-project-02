
import time

import pygame
from futeamigos.environment import RobotTagEnv
from futeamigos.environment.deterministic_agent import Agent
from futeamigos.visualization.drawables import CELL_SIZE, Text, ValueMap
from futeamigos.visualization.frame import take_world_snapshot
from futeamigos.visualization.interpolation import InterpolateFrames


class VisualizeTrained:
    """
    Visualizes a series of episodes given two trained agents

    Args;
        player_1_agent: the agent that controls player 1
        player_2_agent: the agent that controls player 2
        env: the env instance
        player_1_valuemap: if True, shows the value map for the actions around
            the player 1
        player_2_valuemap: if True, shows the value map for the actions around
            the player 2
        ignore_last_position: if True, ignore last position in state
    """

    def __init__(self, player_1_agent: Agent, player_2_agent: Agent,
                 env: RobotTagEnv, player_1_valuemap: bool = False,
                 player_2_valuemap: bool = False,
                 ignore_last_position: bool = False):
        self.env = env

        pygame.init()
        pygame.font.init()
        Text.load_font()

        self.screen_width = self.env.world.width * CELL_SIZE
        self.screen_height = self.env.world.height * CELL_SIZE

        self.screen = pygame.display.set_mode((
            self.screen_width,
            self.screen_height
        ))

        self.player_1_agent = player_1_agent
        self.player_2_agent = player_2_agent

        self.player_1_valuemap = player_1_valuemap
        self.player_2_valuemap = player_2_valuemap

        self.ignore_last_position = ignore_last_position

    def render(self, episodes: int):
        """
        Renders some episodes

        Args:
            episodes: the number of episodes to be rendered
        """
        for _ in range(episodes):
            observation = self.env.reset()
            done = False

            frame_a = take_world_snapshot(world=self.env.world)

            while not done:

                observation, _, done, _ = self.env.step(
                    actions=(
                        self.player_1_agent.get_action(
                            observation=observation,
                            deterministic=True
                        ),
                        self.player_2_agent.get_action(
                            observation=observation,
                            deterministic=True
                        )
                    )
                )

                frame_b = take_world_snapshot(world=self.env.world)

                frame_interpolation = InterpolateFrames(
                    frame_a=frame_a,
                    frame_b=frame_b,
                    grid_height=self.env.world.height,
                    grid_width=self.env.world.width
                )

                for frame in frame_interpolation:
                    time.sleep(1/120)

                    self.screen.blit(source=frame, dest=(0, 0))

                    if self.player_1_valuemap:
                        ValueMap.draw(
                            player_position=frame_a.player_1_position,
                            player_last_direction=frame_a.player_1_last_direction,
                            other_player_position=frame_a.player_2_position,
                            other_player_last_direction=frame_a.player_2_last_direction,
                            screen=self.screen,
                            agent=self.player_1_agent,
                            ignore_last_position=self.ignore_last_position,
                            wall_positions=frame_a.walls_position,
                            player_number=1
                        )

                    if self.player_2_valuemap:
                        ValueMap.draw(
                            player_position=frame_a.player_2_position,
                            player_last_direction=frame_a.player_2_last_direction,
                            other_player_position=frame_a.player_1_position,
                            other_player_last_direction=frame_a.player_2_last_direction,
                            screen=self.screen,
                            agent=self.player_2_agent,
                            ignore_last_position=self.ignore_last_position,
                            wall_positions=frame_a.walls_position,
                            player_number=2
                        )

                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            return

                    pygame.event.clear()
                    pygame.display.flip()

                time.sleep(0.4)

                frame_a = frame_b


if __name__ == '__main__':
    from futeamigos.environment import DeterministicFollowerAgent
    from futeamigos.environment.grids import GRID_ROTATION
    from futeamigos.environment.robot_tag_v3 import RobotTagV3

    env = RobotTagV3.from_grids(grids=GRID_ROTATION)

    follower_agent_2 = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_2,
        enemy=env.world.player_1
    )

    follower_agent_1 = DeterministicFollowerAgent(
        world=env.world,
        me=env.world.player_1,
        enemy=env.world.player_2
    )

    visualizer = VisualizeTrained(
        player_1_agent=follower_agent_1,
        player_2_agent=follower_agent_2,
        player_1_valuemap=True,
        env=env
    )

    visualizer.render(episodes=10)
