from abc import ABC, abstractclassmethod
from typing import List, Tuple

import futeamigos
import numpy as np
import pygame
from futeamigos.environment import CardinalDirections
from futeamigos.environment.deterministic_agent import Agent
from futeamigos.environment.robot_tag_v3 import Slices
from futeamigos.visualization.frame import Position
from pygame import Surface

GROUND_COLOR = (221, 233, 175)
LINE_COLOR = (170, 222, 135)
CELL_SIZE = 48
WALL_COLOR = (205, 222, 135)


class Drawable(ABC):
    """
    Generic interface for Drawable elements
    """

    @abstractclassmethod
    def draw(cls, screen: Surface):
        """
        Generic interface to draw in a surface
        """


class Grid(Drawable):
    """
    Draws the grid on a surface, including lines
    """

    @classmethod
    def draw(cls, grid_width: int, grid_height: int, screen: Surface):
        """
        Draw the grid in a surface

        Args:
            grid_width: the number of columns in the grid
            grid_height: the number of lines in the grid
        """
        screen.fill(GROUND_COLOR)

        width = grid_width * CELL_SIZE
        height = grid_height * CELL_SIZE

        for x in range(0, width, CELL_SIZE):
            pygame.draw.line(
                surface=screen,
                color=LINE_COLOR,
                start_pos=(x, 0),
                end_pos=(x, height)
            )

        for y in range(0, height, CELL_SIZE):
            pygame.draw.line(
                surface=screen,
                color=LINE_COLOR,
                start_pos=(0, y),
                end_pos=(width, y)
            )


class Character(Drawable):
    """
    Draws a character on the screeen
    """

    SURFACE: pygame.Surface = None
    SOURCE: str = futeamigos.__path__[0] + '/visualization/sprites/'

    @classmethod
    def get_cell_center(cls, grid_x: int, grid_y: int) -> Tuple[int, int]:
        """
        Returns the screen position (with a reference on the center) of a given
        cell in the grid

        Args:
            grid_x: the x position in the grid
            grid_y: the y position in the grid
        """
        x = grid_x * CELL_SIZE
        y = grid_y * CELL_SIZE

        return (int(x + CELL_SIZE/2), int(y + CELL_SIZE/2))

    @classmethod
    def match_image_center(cls, cell_center_x: int, cell_center_y: int) \
            -> Tuple[int, int]:
        """
        Centralizes the image center with a cell center

        Args:
            cell_center_x: the cell center x position in the screen
            cell_center_y: the cell center y position in the screen
        """
        x = int(cell_center_x - cls.SURFACE.get_width()/2)
        y = int(cell_center_y - cls.SURFACE.get_height()/2)

        return x, y

    @classmethod
    def from_grid_to_screen_position(cls, grid_x: int, grid_y: int) \
            -> Tuple[int, int]:
        """
        Casts a grid position to a screen position

        Args:
            grid_x: the x position in the grid
            grid_y: the y position in the grid
        """
        x, y = cls.get_cell_center(grid_x=grid_x, grid_y=grid_y)
        x, y = cls.match_image_center(cell_center_x=x, cell_center_y=y)

        return x, y

    @classmethod
    def draw(cls, x: int, y: int, screen: pygame.Surface):
        """
        Draws a character on the screen

        Args:
            x: the x position in the screen
            y: the y position in the screen
        """
        screen.blit(
            source=cls.SURFACE,
            dest=(x, y)
        )


class Wall(Drawable):
    """
    Draws a wall on a surface
    """

    @classmethod
    def draw(cls, grid_x: int, grid_y: int, screen: pygame.Surface):
        """
        Draws a wall on a surface

        Args:
            grid_x: the x position of the wall in the grid
            grid_y: the y position of the wall in the grid
        """
        x = grid_x * CELL_SIZE
        y = grid_y * CELL_SIZE

        pygame.draw.rect(
            surface=screen,
            color=WALL_COLOR,
            rect=pygame.Rect(x, y, CELL_SIZE, CELL_SIZE)
        )


class Text(Drawable):
    """
    Draws text on a surface
    """
    FONT = None

    @classmethod
    def load_font(cls):
        """
        Loads font information (call after pygame.font.init)        
        """
        cls.FONT = pygame.font.SysFont('Arial', 8)

    @classmethod
    def draw(cls, x: int, y: int, text: str, screen: pygame.Surface):
        """
        Draws text on the given position

        Args:
            x: the x position
            y: the y position
            text: the text that will be displayed
            screen: the surface where the text will be displayed
        """
        surface = cls.FONT.render(text, True, (0, 0, 0))

        screen.blit(
            source=surface,
            dest=(
                x - surface.get_width()/2,
                y - surface.get_height()/2
            )
        )


class ValueMap(Drawable):
    """
    Draws a ValueMap around a player given the value of the actions
    """

    @classmethod
    def get_observation(
        cls,
        player_position: Tuple[int, int],
        player_last_direction: int,
        other_player_position: Tuple[int, int],
        other_player_last_direction: int,
        player_number: int,
        wall_positions: List[Position],
        ignore_last_position: bool = False) \
            -> np.array:
        """
        Returns the env observation given the players position

        Args:
            player_position: the player (which is being evaluated for the
                ValueMap) position
            player_last_direction: the player last action
            other_player_position: the position of the other player
            other_player_last_direction: the other player last direction
            player_number: the player position in the observation tuple
            wall_positions: a list of wall positions
            ignore_last_position: if True, ignore last position in state
        """
        # NOTE: player number was an incorrect assumption when developing
        # this module
        # NOTE-2: now it is usefull
        # NOTE-3: this code sucks

        other_player_number = 1 if player_number == 2 else 2

        grid = np.zeros((3, 10, 10))

        for wall in wall_positions:
            grid[Slices.WALL_SLICE][wall.y][wall.x] = 1

        grid[player_number][player_position[0]][player_position[1]] = 1
        grid[other_player_number][other_player_position[0]
                                  ][other_player_position[1]] = 1

        return np.array(grid)

    @classmethod
    def get_values(cls,
                   player_position: Tuple[int, int],
                   player_last_direction: int,
                   other_player_position: Tuple[int, int],
                   other_player_last_direction: int,
                   agent: Agent,
                   player_number: int,
                   wall_positions: List[Position],
                   ignore_last_position: bool = False) -> List[float]:
        """
        Gets the action values around the player

        Args:
            player_position: the player (which is being evaluated for the
                ValueMap) position
            player_last_direction: the player last action
            other_player_position: the position of the other player
            other_player_last_direction: the other player last direction
            agent: the agent that controls the player
            player_number: the player position in the observation tuple
            wall_positions: a list of wall positions
            ignore_last_position: if True, ignore last position in state
        """
        values = []

        for action in CardinalDirections():
            values.append(
                agent.get_value(
                    observation=cls.get_observation(
                        player_position=player_position,
                        player_last_direction=player_last_direction,
                        other_player_position=other_player_position,
                        other_player_last_direction=other_player_last_direction,
                        player_number=player_number,
                        ignore_last_position=ignore_last_position,
                        wall_positions=wall_positions
                    ),
                    action=action
                )
            )

        return values

    @classmethod
    def draw(cls,
             player_position: Tuple[int, int],
             player_last_direction: int,
             other_player_position: Tuple[int, int],
             other_player_last_direction: int,
             screen: pygame.Surface,
             wall_positions: List[Position],
             agent: Agent, player_number: int = 0,
             ignore_last_position: bool = False):
        """
        Draws the valuemap around the player

        Args:
            player_position: the player (which is being evaluated for the
                ValueMap) position
            player_last_direction: the player last action
            other_player_position: the position of the other player
            other_player_last_direction: the other player last direction
            screen: the surface where the valuemap will be drawed
            agent: the agent that controls the player
            wall_positions: a list of wall positions
            player_number: the player position in the observation tuple
            ignore_last_position: if True, ignore last position in state
        """
        surface = pygame.Surface(
            size=(3 * CELL_SIZE, 3 * CELL_SIZE),
            flags=pygame.SRCALPHA
        )

        values = cls.get_values(
            player_position=player_position,
            player_last_direction=player_last_direction,
            other_player_position=other_player_position,
            other_player_last_direction=other_player_last_direction,
            agent=agent,
            player_number=player_number,
            ignore_last_position=ignore_last_position,
            wall_positions=wall_positions
        )

        max_value = max(values) + 1e-5
        values = iter(values)

        for line in range(0, 3):
            for column in range(0, 3):
                x = column * CELL_SIZE
                y = line * CELL_SIZE

                value = next(values)
                normalized_value = (value/max_value) * 64 + 10

                normalized_value = min(normalized_value, 255)
                normalized_value = max(normalized_value, 0)

                if value == max_value - 1e-5:
                    color = (30, 150, 50)

                elif value < 0:
                    color = (225, 75, 75)
                    normalized_value = 128

                else:
                    color = (0, 0, 0)

                pygame.draw.rect(
                    surface=surface,
                    color=(*color, normalized_value),
                    rect=pygame.Rect(x, y, CELL_SIZE, CELL_SIZE)
                )

                Text.draw(
                    x=x + CELL_SIZE/2,
                    y=y + CELL_SIZE/2,
                    text='{:.2f}'.format(value),
                    screen=surface
                )

        screen.blit(
            source=surface,
            dest=(
                (player_position[0] - 1) * CELL_SIZE,
                (player_position[1] - 1) * CELL_SIZE
            )
        )


class Player1(Character):
    """
    Renders the Player 1
    """

    SURFACE = pygame.image.load(Character.SOURCE + 'player_1.png')


class Player2(Character):
    """
    Renders the Player 2
    """

    SURFACE = pygame.image.load(Character.SOURCE + 'player_2.png')
