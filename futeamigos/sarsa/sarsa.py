import itertools
import pickle
import time
import numpy as np
from os import system
from collections import defaultdict
from abc import ABC, abstractmethod
from typing import Dict, Tuple
from futeamigos.sarsa.table import Table
from futeamigos.environment import RobotTagEnv
from futeamigos.environment.grids import *
from futeamigos.environment import DeterministicFollowerAgent
from futeamigos.environment.character import CharacterObservation
from futeamigos.environment.directions import CardinalDirections
from futeamigos.environment.world import Character, World


class Agent(ABC):
    """
    An abstract class representing an agent
    """

    @abstractmethod
    def get_action(self, observation: CharacterObservation) -> int:
        """
        Return an action given an observation

        Args:
            observation: the observation to which the agent should react
        """

    @abstractmethod
    def get_value(self, observation: CharacterObservation, action: int) \
            -> float:
        """
        Get the value of a given observation

        Args:
            observation: the world state being observed by the agent
        """


class SarsaAgent(Agent):
        def __init__(self, epsilon: float, alpha: float, gamma: float,
            trace_decay: float,  height: int, width: int, num_actions: int):
            self.num_actions = num_actions
            self.epsilon = epsilon
            self.alpha = alpha
            self.gamma = gamma
            self.trace_decay = trace_decay
            self.num_actions = num_actions
            self.Q = Table(width=width, height=height)
            self.E = Table(width=width, height=height) #Elegibility traces


        def epsilon_greedy_policy(self, observation: CharacterObservation) -> np.array:
            """
                Returns all action probabilities based on epsilon-greedy exploration
                algorithm.
            """
            m = self.num_actions
            A = np.ones(m, dtype=float) * self.epsilon/m
            best_action = self.Q.get_max_action(**observation_to_kwarg(observation=observation))
            #best_action = np.argwhere(self.Q[observation] == np.amax(self.Q[observation]))
            #print('best action (greedy) ', best_action)

            #if(len(best_action) > 1):
            #    best_action = np.random.choice(np.array(best_action).squeeze()) #break tie
            A[best_action] += (1.0 - self.epsilon)
            return A


        def get_action(self, observation: CharacterObservation, deterministic=False) -> int:
            """
                Returns a CardinalDirection (int) indicating where the agent should
                move to in order to tag the enemy.
            """
            choice = -1
            if(not deterministic):
                next_action_probs = self.epsilon_greedy_policy(observation)
                choice = np.random.choice(np.arange(len(next_action_probs)), p=next_action_probs)
            else:
                choice = self.Q.get_max_action(**observation_to_kwarg(observation=observation))
            next_action = CardinalDirections.DESLOC_TO_VECTOR[choice]
            return CardinalDirections.VECTOR_TO_DESLOC[next_action]


        def get_value(self, observation: CharacterObservation, action: int) -> float:
            return self.Q.get_value(
                **observation_to_kwarg(observation=observation),
                action=action)


        def update(self, state, action, reward, next_state, next_action):
            print('state ', state)
            print('action ', action)
            print('Q ', self.get_value(state, action))
            print('reward ', reward)
            print('next state ', next_state)
            print('next action ', next_action)
            print('next Q ', self.get_value(next_state, next_action))
            delta = reward + self.gamma*self.get_value(next_state, next_action)\
                - self.get_value(state, action) # delta = R+gamma*Q'-Q
            #print('delta ', delta)
            '''
            E = self.E.get_value(**observation_to_kwarg(state), action=action)
            self.E.set_value(
                **observation_to_kwarg(state),
                action=action,
                value=E+1)
            '''
            Q = self.get_value(state, action) + self.alpha*delta
            self.Q.set_value(
                **observation_to_kwarg(state),
                action=action,
                value=Q)
            '''
            self.E.set_value(
                **observation_to_kwarg(state),
                action=action,
                value=(E+1)*self.trace_decay*self.gamma)
            '''
            #for s in self.Q:
            #    self.Q[s][:] += self.alpha * delta * self.E[s][:] #Q = Q+alpha*delta*E
            #    self.E[s][:] *= self.trace_decay * self.gamma #E = E*gamma*lambda


def observation_to_kwarg(observation: Tuple[int]) -> Dict[str, int]:
    """
    """
    return dict(
        agent_x=observation[0],
        agent_y=observation[1],
        agent_last_direction=observation[2],
        enemy_x=observation[3],
        enemy_y=observation[4],
        enemy_last_direction=observation[5]
    )


def evaluate(env, my_agent, enemy, n_steps):
    reward_history = []
    for sub_ep in range(n_steps):
        observation = env.reset()
        done = False
        steps = 0
        rewards = []

        while steps < 100 and not done:
            system('clear')
            print('Evaluating ')
            env.render()
            #time.sleep(0.01)
            action = my_agent.get_action(
                observation=observation,
                deterministic=True)

            observation, reward, done, _ = env.step(actions=(action,
                enemy.get_action(observation=observation)))
            steps += 1
            rewards.append(reward)
        reward_history.append(sum(rewards))
    return reward_history


def my_reward(world: World) -> float:
    if(world.player_1.touched(world.player_2)):
        return -1
    return 1


def train(max_steps, max_episodes):
    rewards = [None]
    env = RobotTagEnv.from_grid(grid=EMPTY_GRID)
    height = len(EMPTY_GRID)
    width = len(EMPTY_GRID[0])
    env.reward_function = my_reward#lambda world: 1
    my_agent = SarsaAgent(epsilon=0.3, alpha=0.01, gamma=0.9, trace_decay=0.9,
        num_actions=9, width=width, height=height)

    follower_agent = DeterministicFollowerAgent(
        world = env.world,
        me = env.world.player_2,
        enemy = env.world.player_1
    )

    for episode in range(max_episodes):
        observation = env.reset()
        #print('observation ', observation)
        action = my_agent.get_action(observation)
        state = observation
        done = False
        ep_reward = 0
        n_steps = 0

        if(episode % 100 == 0):
            print('Evaluation')
            reward_history = evaluate(env, my_agent, follower_agent, 20)
            with open('../../output/rewards_eval.csv', 'a') as csv_file:
                csv_file.write(f"{sum(reward_history)/20}\n")
            print('Eval rewards:', reward_history, sum(reward_history)/20)

        while not done:
            system('clear')
            print('Episode {}/{} Last Reward {}'.format(episode, max_episodes, rewards[-1]))
            env.render()
            enemy_action = follower_agent.get_action(observation)
            observation, reward, done, world = env.step(actions=(action, enemy_action))
            next_state = observation
            next_action = my_agent.get_action(next_state)
            my_agent.update(state, action, reward, next_state, next_action)

            if(n_steps == max_steps):
                break

            ep_reward += reward
            n_steps+=1
            state = next_state
            action = next_action
        rewards.append(ep_reward)
    my_agent.Q.save('sarsa_table')
    #return my_agent.Q, rewards


def test(env, Q, max_episodes):
    follower_agent = DeterministicFollowerAgent(
        world = env.world,
        me = env.world.player_2,
        enemy = env.world.player_1
    )
    my_agent = SarsaAgent(epsilon=0.1, alpha=0.01, gamma=0.9, trace_decay=0.9,
        num_actions=9)

    observation = env.reset()
    for i in range(max_episodes):
        system('clear')
        print(my_agent.Q[observation])
        env.render()
        time.sleep(0.5)

        my_action = my_agent.get_action(observation, deterministic=True)
        enemy_action = follower_agent.get_action(observation)
        observation, reward, done, world = env.step(actions=(my_action,
            enemy_action))


if __name__== '__main__':
    start = time.time()
    train(100, 50000)
    print('training took ', time.time()-start, 's')

    #np.save('../../output/state_action_values.npy', np.array(dict(Q)))
    #np.save('../../output/rewards.npy', np.array(rewards))

    #import matplotlib.pyplot as plt
    #rewards = np.load('../output/rewards.npy', allow_pickle=True)[1:]
    #plt.plot(range(len(rewards)), rewards)
    #plt.show()

    #Q_test = np.load('../../output/state_action_values.npy', allow_pickle=True).item()
    #test(env=RobotTagEnv.from_grid(grid=EMPTY_GRID), Q=Q_test, max_episodes=100)
