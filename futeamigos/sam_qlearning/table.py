import pickle
from typing import List

import numpy as np
from futeamigos.environment import CardinalDirections


class Table:
    def __init__(self, height: int, width: int):
        """
        """
        num_actions = len(list(CardinalDirections()))

        self.table = np.zeros(
            (
                num_actions,
                width,
                height,
                width,
                height
            )
        )

    def get_value(
            self,
            agent_x: int,
            agent_y: int,
            enemy_x: int,
            enemy_y: int,
            action: int) -> float:
        """
        """
        return self.table[
            action,
            agent_x,
            agent_y,
            enemy_x,
            enemy_y,
        ]

    def set_value(
            self,
            agent_x: int,
            agent_y: int,
            enemy_x: int,
            enemy_y: int,
            action: int,
            value: float) -> float:
        """
        """
        self.table[
            action,
            agent_x,
            agent_y,
            enemy_x,
            enemy_y,
        ] = value

    def get_max_value(
            self,
            agent_x: int,
            agent_y: int,
            enemy_x: int,
            enemy_y: int) -> float:
        """
        """
        return np.max(
            self.table[
                :,
                agent_x,
                agent_y,
                enemy_x,
                enemy_y,
            ]
        )

    def get_max_action(
            self,
            agent_x: int,
            agent_y: int,
            enemy_x: int,
            enemy_y: int) -> float:
        """
        """
        return np.argmax(
            self.table[
                :,
                agent_x,
                agent_y,
                enemy_x,
                enemy_y,
            ]
        )

    def get_all_values(
            self,
            agent_x: int,
            agent_y: int,
            enemy_x: int,
            enemy_y: int) -> np.array:
        """
        """
        return self.table[
            :,
            agent_x,
            agent_y,
            enemy_x,
            enemy_y,
        ]

    def save(self):
        with open('table_file', 'wb') as table_file:
            pickle.dump(self, file=table_file)
